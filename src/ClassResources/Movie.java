package ClassResources;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Movie {
    private String movieName;
    private MovieType movieType;
    private LocalDate releaseDate;
    private String directorName;
    private Boolean isWatched;

    public Movie(String movieName, MovieType movieType, LocalDate releaseDate, String directorName, Boolean isWatched) {
        this.movieName = movieName;
        this.movieType = movieType;
        this.releaseDate = releaseDate;
        this.directorName = directorName;
        this.isWatched = isWatched;
    }

    public String getMovieName() {
        return movieName;
    }


    public MovieType getMovieType() {
        return movieType;
    }


    public LocalDate getReleaseDate() {
        return releaseDate;
    }


    public String getDirectorName() {
        return directorName;
    }

    public Boolean isWatched() {
        return isWatched;
    }

    public void setWatched(Boolean watched) {
        isWatched = watched;
    }

    @Override
    public String toString() {
        return "MovieName: " + movieName + "\nMovieType: " + movieType + "\nYear: " + releaseDate + "\nDirectorName: " + directorName + "\nObejrzany: " + isWatched;
    }
}
