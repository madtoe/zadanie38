package Main;

import javax.swing.*;
import java.awt.*;

public class MovieDatabaseFrame extends JFrame {

    private MovieDatabaseLayout interfejs;

    public MovieDatabaseFrame() throws HeadlessException {
        this.interfejs = new MovieDatabaseLayout();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setContentPane(interfejs.getPanel1());
        pack();
    }
}
