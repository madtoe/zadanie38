package Main;

import ClassResources.Movie;
import ClassResources.MovieDatabase;
import ClassResources.MovieType;
import javafx.concurrent.Task;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MovieDatabaseLayout {
    private JPanel panel1;
    private JTextField tytuł;
    private JTextField director;
    private JComboBox wybierzGatunek;
    private JLabel Reżyser;
    private JTable table;
    private JComboBox wybierzRok;
    private JButton dodaj;
    private JButton usun;
    private JButton odczytaj;
    private JButton zapisz;
    private JCheckBox isWatched;
    private JProgressBar pasek;
    private MovieDatabaseTableModel model;
    private int zaznaczonyIndex[];
    File file = new File("movieDatabase.txt");
    Task task;


    public static boolean printToFile(File file, MovieDatabase movieDatabase) {
        try (PrintWriter pw = new PrintWriter(new FileWriter(file, false))) {
            for (Movie movie : movieDatabase.getMovieMapByName().values()) {
                pw.println(movie);
                pw.println("-separator-");

            }
            return true;
        } catch (IOException e) {
            return false;
        }
    }


    public MovieDatabaseLayout() {
        this.model = new MovieDatabaseTableModel();
        table.setModel(model);
        table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        table.setCellSelectionEnabled(true);
        table.setAutoCreateRowSorter(true);
        ListSelectionModel listSelectionModel = table.getSelectionModel();
        listSelectionModel.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                zaznaczonyIndex = table.getSelectedRows();
                if (zaznaczonyIndex.length == -1) {
                    usun.setEnabled(false);
                } else {
                    usun.setEnabled(true);
                }
                System.out.println("Zaznaczony:" + zaznaczonyIndex);
            }
        });
        dodaj.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DateTimeFormatter dtf = new DateTimeFormatterBuilder().appendPattern("yyyy")
                        .parseDefaulting(ChronoField.MONTH_OF_YEAR, 1)
                        .parseDefaulting(ChronoField.DAY_OF_MONTH, 1)
                        .toFormatter();
                LocalDate releaseDate = LocalDate.parse(String.valueOf(wybierzRok.getSelectedItem()), dtf);
                Movie m = new Movie(tytuł.getText(), MovieType.valueOf(String.valueOf(wybierzGatunek.getSelectedItem()).toUpperCase()), releaseDate, director.getText(), isWatched.isSelected());
                model.addMovie(m);
            }
        });
        zapisz.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printToFile(file, MovieDatabase.INSTANCE);
            }
        });
        odczytaj.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                odczytaj.setEnabled(false);
                odczytaj.setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));
                ProgressBarTask task = new ProgressBarTask();
                task.done = false;
                task.addPropertyChangeListener(new PropertyChangeListener() {
                    @Override
                    public void propertyChange(PropertyChangeEvent evt) {
                        if ("progress" == evt.getPropertyName()) {
                            int progress = (Integer) evt.getNewValue();
                            pasek.setValue(progress);
                        }
                    }
                });
                task.execute();
            }
        });
        usun.setEnabled(false);

        usun.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                List<String> lista = new ArrayList<>();

                for (int i = 0; i < zaznaczonyIndex.length; i++) {
                    lista.add(model.getMap().get(model.getMap().keySet().toArray()[zaznaczonyIndex[i]]).getMovieName());
                }
                for (String s : lista) {
                    model.getMap().remove(s);
                }
                model.fireTableDataChanged();

            }
        });
        model.addTableModelListener(new TableModelListener() {

            @Override
            public void tableChanged(TableModelEvent e) {
                if (e.getColumn() == 0 && e.getFirstRow() > -1) {
                    System.out.println("Row : " + e.getFirstRow() +
                            " value :" + model.getValueAt(e.getFirstRow(), e.getColumn()));
                }
            }
        });


        odczytaj.addActionListener(new ActionListener() {


            @Override
            public void actionPerformed(ActionEvent evt) {
                odczytaj.setEnabled(false);
                odczytaj.setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));
                ProgressBarTask task = new ProgressBarTask();
                task.done = false;
                task.addPropertyChangeListener(new PropertyChangeListener() {
                    @Override
                    public void propertyChange(PropertyChangeEvent evt) {
                        if ("progress" == evt.getPropertyName()) {
                            int progress = (Integer) evt.getNewValue();
                            pasek.setValue(progress);
                        }
                    }
                });
                task.execute();
            }

        });
        isWatched.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
            }
        });


    }

    public JPanel getPanel1() {
        return panel1;
    }

    public JTextField getTytuł() {
        return tytuł;
    }


    public JTable getTable() {
        return table;
    }


    public JTextField getDirector() {
        return director;
    }


    public void setData(Movie data) {
    }

    public void getData(Movie data) {
    }

    public boolean isModified(Movie data) {
        return false;
    }

    class ProgressBarTask extends javax.swing.SwingWorker<Void, Void> {
        boolean done;

        public void done() {
            done = true;
//            Toolkit.getDefaultToolkit().beep();
            odczytaj.setEnabled(true);
            odczytaj.setCursor(null);
            pasek.setValue(pasek.getMinimum());
        }

        @Override
        protected Void doInBackground() {
            setProgress(0);
            for (int i = 0; i < 101; i++) {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                setProgress(i);
            }
            model.clear();
            try {
                Scanner fileScan = new Scanner(file);
                Parser readParser = new Parser();
                while (fileScan.hasNextLine()) {
                    String line = fileScan.nextLine();
                    String[] lineArguments = line.split(":");
                    String keyword = lineArguments[0];

                    if (keyword.contains("MovieName")) {
                        readParser.setMovieName(lineArguments[1].trim());

                    } else if (keyword.contains("MovieType")) {
                        readParser.setMovieType(MovieType.valueOf(lineArguments[1].trim()));
                    } else if (keyword.contains("Year")) {
                        readParser.setReleaseDate(LocalDate.parse(lineArguments[1].trim()));
                    } else if (keyword.contains("DirectorName")) {
                        readParser.setDirectorName(lineArguments[1].trim());
                    } else if (keyword.contains("Obejrzany")) {
                        if (lineArguments[1].trim().contains("true"))
                            readParser.setWatched(true);
                        else readParser.setWatched(false);
                    } else if (keyword.contains("separator")) {
                        MovieDatabase.INSTANCE.addMovie(readParser);
                    }
                }
            } catch (FileNotFoundException fe) {
                fe.printStackTrace();
            }
            model.fireTableDataChanged();
            return null;
        }
    }

}
