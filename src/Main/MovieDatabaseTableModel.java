package Main;

import ClassResources.Movie;
import ClassResources.MovieDatabase;

import javax.swing.table.AbstractTableModel;
import java.util.Map;


public class MovieDatabaseTableModel extends AbstractTableModel {
    private final String[] columnNames =
            new String[]{"Tytuł", "Gatunek", "Data Premiery", "Reżyser", "Obejrzany"};
    private Map<String, Movie> map = MovieDatabase.INSTANCE.getMovieMapByName();

    public MovieDatabaseTableModel() {

    }

    public void clear() {
        map.clear();
    }

    public void addMovie(Movie m) {
        map.put(m.getMovieName(), m);
        fireTableDataChanged();
    }

    public void removeMovie(Movie m) {
        map.remove(m.getMovieName(), m);
        fireTableDataChanged();
    }

    @Override
    public String getColumnName(int column) {
        return this.columnNames[column];
    }

    @Override
    public int getRowCount() {
        return map.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Movie tmp = map.get(map.keySet().toArray()[rowIndex]);
        switch (columnIndex) {
            case 0: // co ma wyświetlić kolumna 0
                return tmp.getMovieName();
            case 1: // co ma wyświetlić kolumna 1
                return tmp.getMovieType();
            case 2: // co ma wyświetlić kolumna 2
                return tmp.getReleaseDate();
            case 3: // co ma wyświetlić kolumna 3
                return tmp.getDirectorName();
            case 4:
                return tmp.isWatched();
            default: // co ma wyświetlić każda inna kolumna
                return "unknown";

        }
    }

    @Override
    public void setValueAt(Object aValue, int row, int col) {
        Movie tmp = map.get(map.keySet().toArray()[row]);
        Boolean checked = (Boolean) getValueAt(row, col);
        if (col == 4 && !checked) {
            System.out.println( ": " + true);
            tmp.setWatched(true);

        } else {
            System.out.println( ": " + false);
            tmp.setWatched(false);

        }
        fireTableRowsUpdated(row, row);

    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (columnIndex == 4) {
            return true;
        }
        return false;

    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {

        switch (columnIndex) {
            case 0: // co ma wyświetlić kolumna 0
                return String.class;
            case 1: // co ma wyświetlić kolumna 1
                return Enum.class;
            case 2: // co ma wyświetlić kolumna 2
                return Integer.class;
            case 3: // co ma wyświetlić kolumna 3
                return String.class;
            case 4:
                return Boolean.class;
            default: // co ma wyświetlić każda inna kolumna
                return String.class;
        }
    }

    public Map<String, Movie> getMap() {
        return map;
    }
}
