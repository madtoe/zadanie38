package Main;

import ClassResources.Movie;
import ClassResources.MovieDatabase;
import ClassResources.MovieType;

import java.io.*;
import java.time.LocalDate;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {


        Scanner scan = new Scanner(System.in);
        File file = new File("movieDatabase.txt");
        boolean isWorking = true;
//        if (readFileIntoDatabase(file, MovieDatabase.INSTANCE)) {
//            System.out.println("File loaded.");
//
//        } else System.out.println("File not read.");
        MovieDatabaseFrame movieDatabaseFrame = new MovieDatabaseFrame();
        movieDatabaseFrame.setVisible(true);
        Perform(scan, MovieDatabase.INSTANCE, isWorking);
        printToFile(file, MovieDatabase.INSTANCE);
    }

    private static void Perform(Scanner scan, MovieDatabase movieDatabase, boolean isWorking) {
        printOptions();
        while (isWorking) {
            String inputLine = scan.nextLine();
            Parser parser = new Parser();
            if (parser.parse(inputLine)) {
                if (parser.getCommandType().equals(CommandType.QUIT)) {
                    isWorking = false;
                }
                if (parser.getCommandType().equals(CommandType.ADD)) {
                    if (!movieDatabase.getMovieMapByName().containsKey(parser.getMovieName())) {
                        movieDatabase.addMovie(parser);
                    } else System.out.println("This movie already exists in the database.");
                } else if (parser.getCommandType().equals(CommandType.PRINTALL)) {
                    movieDatabase.printAllMovies();

                } else if (parser.getCommandType().equals(CommandType.PRINTTYPE)) {
                    movieDatabase.printAllMovies(parser.getMovieType());
                } else if (parser.getCommandType().equals(CommandType.SEARCH)) {
                    String name = parser.getMovieName();
                    if (movieDatabase.getMovieMapByName().containsKey(name)) {
                        System.out.println(movieDatabase.searchByName(parser.getMovieName()));
                    } else {
                        System.out.println("Movie doesn't exist in database");
                    }


                }
            } else {
                System.out.println("Illegal Command or parameters");
            }
        }
    }

    private static void printOptions() {
        System.out.println("Options:");
        System.out.println("-> ADD + movieName + movieType [HORROR/COMEDY/ACTION/DRAMA] + year[yyyy] + DirectorName");
        System.out.println("-> SEARCH + movieName");
        System.out.println("-> PRINTALL");
        System.out.println("-> PRINTTYPE + TYPE");
        System.out.println("-> QUIT");
    }

    public static boolean printToFile(File file, MovieDatabase movieDatabase) {
        try (PrintWriter pw = new PrintWriter(new FileWriter(file, false))) {
            for (Movie movie : movieDatabase.getMovieMapByName().values()) {
                pw.println(movie);
                pw.println("-separator-");
            }
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public static boolean readFileIntoDatabase(File file, MovieDatabase movieDatabase) {
        try {
            Scanner fileScan = new Scanner(file);
            Parser readParser = new Parser();
            while (fileScan.hasNextLine()) {
                String line = fileScan.nextLine();
                String[] lineArguments = line.split(":");
                String keyword = lineArguments[0];

                if (keyword.contains("MovieName")) {
                    readParser.setMovieName(lineArguments[1].trim());

                } else if (keyword.contains("MovieType")) {
                    readParser.setMovieType(MovieType.valueOf(lineArguments[1].trim()));
                } else if (keyword.contains("Year")) {
                    readParser.setReleaseDate(LocalDate.parse(lineArguments[1].trim()));
                } else if (keyword.contains("DirectorName")) {
                    readParser.setDirectorName(lineArguments[1].trim());
                } else if (keyword.contains("separator")) {
                    movieDatabase.addMovie(readParser);
                } else return false;
            }
            return true;
        } catch (FileNotFoundException e) {
            return false;
        }

    }
}
