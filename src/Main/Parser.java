package Main;

import ClassResources.MovieType;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;


public class Parser {
    private String movieName;
    private MovieType movieType;
    private LocalDate releaseDate;
    private String directorName;
    private CommandType commandType;
    private boolean isWatched;

    public boolean isWatched() {
        return isWatched;
    }

    public boolean parse(String inputLine) {
        String[] inputArguments = inputLine.split(" ");

        try {
            this.commandType = CommandType.valueOf(inputArguments[0].toUpperCase());
        } catch (IllegalArgumentException e) {
            return false;
        }

        try {
            if(commandType.equals(CommandType.ADD)){
                this.movieName = inputArguments[1];
                this.movieType = MovieType.valueOf(inputArguments[2].toUpperCase());
                DateTimeFormatter dtf = new DateTimeFormatterBuilder().appendPattern("yyyy")
                        .parseDefaulting(ChronoField.MONTH_OF_YEAR, 1)
                        .parseDefaulting(ChronoField.DAY_OF_MONTH, 1)
                        .toFormatter();
                this.releaseDate = LocalDate.parse(inputArguments[3], dtf);
                this.directorName = inputArguments[4];
            }
            else if(commandType.equals(CommandType.PRINTTYPE)){
                this.movieType = MovieType.valueOf(inputArguments[1].toUpperCase());
            }
            else if(commandType.equals(CommandType.SEARCH)){
                this.movieName = inputArguments[1];
            }

        } catch (ArrayIndexOutOfBoundsException | IllegalArgumentException e2){
            return false;
        }
        return true;
    }

    public String getMovieName() {
        return movieName;
    }

    public MovieType getMovieType() {
        return movieType;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public String getDirectorName() {
        return directorName;
    }

    public CommandType getCommandType() {
        return commandType;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public void setMovieType(MovieType movieType) {
        this.movieType = movieType;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public void setDirectorName(String directorName) {
        this.directorName = directorName;
    }

    public void setWatched(Boolean watched) {
        isWatched = watched;
    }
}
